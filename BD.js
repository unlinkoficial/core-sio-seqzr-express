import mysql2 from 'mysql2';
import { Sequelize } from 'sequelize';
import './Helper';

class BD {
  constructor() {
    if (typeof BD.instance === 'object') {
      return BD.instance;
    }
    this.sequelize = null;
    BD.instance = this;
  }

  /**
   * Carga la configuración de BD a partir de la configuración
   */
  async init(config) {
    if (this.sequelize !== null) {
      return;
    }
    this.setConnectionParams(config);
    return await this.testConnection();
    
  }
  
  async testConnection() {
    try {
      await this.sequelize.authenticate();
      console.log("conexion exitosa");
      return true;
    } catch (error) {
			console.log("conexion error",error);
      return false;
    }
  }

  /**
 * Establece los parametros de conexión.
 */
  setConnectionParams(config = null) {
		let conexion = { 
			...config, 
			dialect: 'mysql', 
			logging: false,
			dialectOptions: {
        typeCast: function (field, next) {
            if (field.type == 'DATE' ||field.type == 'DATETIME' || field.type == 'TIMESTAMP') {
                return field.string()
            }
            return next();
        }
			},
		};
		if(!config.timezone) {
			conexion['timezone'] = '-06:00';
		}
    if (config === null && this.sequelize !== null) {
      return this.sequelize;
    }
    this.sequelize = new Sequelize({ ...conexion, dialectModule: mysql2 });
    return this.sequelize;
  }
  
}

const bd = new BD();
export default bd;
