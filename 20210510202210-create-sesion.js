'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('sesiones', {
      token: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      user_id: {
				allowNull: false,
        type: Sequelize.BIGINT
      },
      socket_id: {
				allowNull: true,
        type: Sequelize.STRING
      },
      conectado: {
				allowNull: true,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      metainfo: {
				allowNull: true,
        type: Sequelize.STRING
      },
      ultima_vez_conectado: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('sesiones');
  }
};
