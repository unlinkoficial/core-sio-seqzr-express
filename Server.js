import express from 'express';
import * as http from 'http'; // ES 6

export default class Server {
  constructor() {
    if (typeof Server.instance === 'object') {
      return Server.instance;
    }
    this.app = null;
    this.httpServer = null;
    Server.instance = this;
    return this;
  }

  init(config) {
    try {
      this.app = express();
      this.httpServer = http.createServer(this.app);
      let port = config.port_express;
      this.httpServer.listen(port, () => {
				console.log('listening on *:' + port);
			});
			this.bindRoutes();
    } catch (e) {
      //AppError.appendError(e);
    }
  }

  bindRoutes() {
    this.app.get('/', (req, res) => {
			res.send('<h1>Pff</h1>');
			//res.sendFile(__dirname + '/index.html');
		});
    this.app.get('/api-sockets', (req, res) => {
			res.send('<h1>Pff</h1>');
			//res.sendFile(__dirname + '/index.html');
		});
  }
}


